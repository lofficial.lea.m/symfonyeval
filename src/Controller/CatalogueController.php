<?php

namespace App\Controller;

//use App\Repository\CatalogueRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CatalogueController extends AbstractController
{
    /**
     * @Route(
     * "/catalogue",
     *  name="catalogue"
     * )
     */
    public function index(?string $firstname = null):Response
    {
        return $this->render('catalogue/index.html.twig');

    }

//    public function details(CatalogueRepository $catalogueRepository, string $slug):Response
//    {
//        // Sélection d'un produit par son id
//        //$result = $productRepository->find($id);
//
// /*       // formulaire des commentaires
//        $type = CommentType::class;
//        $form = $this->createForm($type);*/
//
//        //sélection d'un livre
////        $result = $catalogueRepository->findOneBy([
////            'slug' => $slug
////        ]);
//
//        return $this->render('product/details.html.twig', [
//            'result' => $result,
//            'form' => $form->createView()
//        ]);
//    }

//    /**
//     * @Route("/products/page/{page}", name="products.index")
//     */
//    public function index(CatalogueRepository $catalogueRepository, int $page):Response
//    {
//
//        //Récupération du nombre de page
//        $nbppage = $this->getParameter('cataloguepage');
//
//
//        // sélection de tous les livres
//        $results = $catalogueRepository->findBy([], [], $nbppage, ($page-1)*$nbppage);
//        $totalresult = $catalogueRepository->findAll();
//        $totalpage = ceil(count($totalresult)/$nbppage);
//        return $this->render('product/index.html.twig', [
//            'results' => $results,
//            'totalpage' => $totalpage
//        ]);
//    }
}